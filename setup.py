from setuptools import find_packages, setup

setup(
    name="recipes_backend",
    version="0.0",

    description="A back-end to demonstrate some useful features of a back-end",

    packages=find_packages('src'),
    package_dir={'': 'src'},

    install_requires=[
        'flask',
        'sqlalchemy',
    ],
)
