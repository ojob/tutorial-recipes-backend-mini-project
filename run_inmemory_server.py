from recipes_backend.app import create_app
app = create_app(with_test_data=True)
app.run()
