# Recipes Back-end Mini-project

This project is my answer to [an exercise in OpenClassrooms 'prepare for interviews' course][1].

[1]: https://openclassrooms.com/fr/courses/6045521/6173226-preparez-vous-aux-mini-projets-de-developpement#/id/r-6173404

So indeed, it's a web back-end, that I can define as follows:

- coded in Python, leveraging some of its recent versions features:

    - [dataclasses][dataclasses-url], new in Python3.7,
    - [typing][typing-url], new in Python3.5;

- using some famous external dependencies: 
    
    - [Flask][flask-url] for handling web requests,
    - [SQLAlchemy][sqlalchemy-url] for handling data persistence,
    - [pytest][pytest-url] for automated tests definition and execution.

[dataclasses-url]: https://docs.python.org/3/library/dataclasses.html
[typing-url]: https://docs.python.org/3/library/typing.html
[flask-url]: https://flask.palletsprojects.com/
[sqlalchemy-url]: https://www.sqlalchemy.org/
[pytest-url]: https://docs.pytest.org/en/latest/contents.html


## Installation

- Clone the repo:

  ```
  cd some/where/that/pleases/you/
  git clone git@framagit.org:ojob/tutorial-recipes-backend-mini-project.git
  cd tutorial-recipes-backend-mini-project
  ```
        
- create and activate a Python virtual environment:

  ```
  python -m venv .env
  source .env/bin/activate
  # don't forget to update pip!
  pip install --upgrade pip
  ```
  
- install the package (this will also install the dependencies):

  ```
  pip install -e .
  ```
  
## Usage

- Launch the back-end:

  ```
  python run_inmemory_server.py
  ```
  
- Then open your favorite web navigator to see the result:

  - root is http://127.0.0.1:5000/
  - the back-end is filled with a first recipe; you can see its
    details at http://127.0.0.1:5000/recipes/0/details.
    

## Test

- Install the test dependencies:

  ```
  pip install -r dev-requirements.txt
  ```
  
- launch the test suite execution. It's a simple command, as all details
  are defined in `pytest.ini` file:

  ```
  pytest
  ```

  This generates a code coverage report; open
  `/html-coverage-report/index.html` file to navigate through the code coverage.
  
