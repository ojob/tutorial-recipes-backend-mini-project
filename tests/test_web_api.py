import json

import pytest

from recipes_backend.app import create_app
from recipes_backend.repository.sql import SQLRepository


def test_client_with_sql_repo(with_test_data: bool = False):
    repo = SQLRepository()
    app = create_app(repo=repo, with_test_data=with_test_data)
    return app.test_client()


def test_client_with_dict_repo(with_test_data: bool = False):
    app = create_app(with_test_data=with_test_data)
    return app.test_client()


@pytest.mark.parametrize(
    'test_client',
    [
        test_client_with_sql_repo(),
        test_client_with_dict_repo(),
    ]
)
def test_index(test_client):
    response = test_client.get('/')
    assert response.status_code == 200


@pytest.mark.parametrize(
    'test_client',
    [
        test_client_with_sql_repo(),
        test_client_with_dict_repo(),
    ]
)
def test_no_recipe(test_client):
    response = test_client.get('/recipes')
    assert response.status_code == 200
    payload = response.json
    assert 'no recipe' in payload['message'].lower()


@pytest.mark.parametrize(
    'test_client',
    [
        test_client_with_sql_repo(),
        test_client_with_dict_repo(),
    ]
)
def test_add_recipe_and_get(test_client):
    recipe_name = 'some recipe'
    response = test_client.post(
        '/recipes',
        data=json.dumps(dict(
            name=recipe_name, description='a new one',
        )),
        content_type='application/json',
    )
    assert response.status_code == 201
    payload = response.json
    assert payload['message'] == 'created'
    recipe_id = payload['data']
    assert isinstance(recipe_id, int)

    response = test_client.get(f'/recipes/{recipe_id}')
    assert response.status_code == 200
    assert response.json['data'] == recipe_name


@pytest.mark.parametrize(
    'test_client',
    [
        test_client_with_sql_repo(),
        test_client_with_dict_repo(),
    ]
)
def test_get_non_existing_recipe(test_client):
    unknown_id = 68468468484684
    response = test_client.get(f'/recipes/{unknown_id}')
    assert response.status_code == 404
    assert response.json['message'] == f'no recipe with id={unknown_id}'


@pytest.mark.parametrize(
    'test_client',
    [
        test_client_with_sql_repo(with_test_data=True),
        test_client_with_dict_repo(with_test_data=True),
    ]
)
def test_get_recipes(test_client):
    response = test_client.get('/recipes')
    assert response.status_code == 200
    assert len(response.json['data']) > 0
    assert 'name' in response.json['data'][0]
    assert 'id' in response.json['data'][0]


@pytest.mark.parametrize(
    'test_client',
    [
        test_client_with_sql_repo(with_test_data=True),
        test_client_with_dict_repo(with_test_data=True),
    ]
)
def test_get_existing_recipe_details(test_client):
    existing_id = test_client.get('/recipes').json['data'][0]['id']
    response = test_client.get(f'/recipes/{existing_id}/details')
    assert response.status_code == 200
    recipe_details = response.json['data']
    assert 'steps' in recipe_details


@pytest.mark.parametrize(
    'test_client',
    [
        test_client_with_sql_repo(with_test_data=True),
        test_client_with_dict_repo(with_test_data=True),
    ]
)
def test_get_non_existing_recipe_details(test_client):
    response = test_client.get('/recipes/197536/details')
    assert response.status_code == 404
