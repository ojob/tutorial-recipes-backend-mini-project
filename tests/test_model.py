import pytest

from recipes_backend import model


def test_recipe_creation_from_dict():
    recipe_data = dict(
        id=1234,
        name="quatre-quarts",
        description="un gâteau qu'on l'aime",
        steps=[
            dict(
                title="préparer les ingrédients",
                description="faire fondre dans une petite casserole",
                ingredients=[
                    dict(name="beurre", quantity=100.0, metric="g"),
                    dict(name="chocolat", quantity=50.0, metric="une cuillère à café")
                ],
                img=None,
            ),
            dict(
                title="préparer le moule",
                description="beurrer le fond du moule",
                ingredients=[
                    dict(name="beurre", quantity=5.0, metric="g"),
                ],
                img=None,
            )
        ]
    )

    recipe = model.Recipe.from_dict(recipe_data)
    assert recipe.name == recipe_data['name']

    assert recipe.ingredients
    assert recipe.ingredients[0] == model.Ingredient('beurre', 105.0, 'g')

    assert recipe.as_dict() == recipe_data


def test_recipe_id_generation():
    recipe_data = dict(
        name="some recipe",
        description="another recipe",
    )
    first_recipe = model.Recipe.from_dict(recipe_data)
    assert model.Recipe.from_dict(recipe_data).id == first_recipe.id + 1
    assert model.Recipe.from_dict(recipe_data).id == first_recipe.id + 2


def test_recipe_random_creation():
    recipe = model.Recipe.random()
    assert recipe.name.startswith('some name')
    assert recipe.description.startswith('random description')


def test_ingredient_additions():
    flour1 = model.Ingredient('flour', 250.0, 'g')
    flour2 = model.Ingredient('flour', 80.0, 'g')
    flour3 = model.Ingredient('flour', 1, 'spoon')
    salt = model.Ingredient('salt', 1, 'pitchenette')

    total_flour = flour1 + flour2
    assert isinstance(total_flour, model.Ingredient)
    assert total_flour.quantity == 330.0
    assert total_flour.metric == 'g'

    with pytest.raises(TypeError):
        flour1 + flour3

    with pytest.raises(TypeError):
        flour2 + salt


