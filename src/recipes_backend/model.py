"""Model definition for recipes.

This provides plain-old Python objects, not tainted by any sort of
persistence consideration: this makes them easy to be tested, and not
likely to be modified if some aspects are modified (data storage, web
requests update, etc.).

"""
from __future__ import annotations

import random
from dataclasses import asdict, dataclass, field
from typing import Dict, List


@dataclass
class Ingredient:
    name: str
    quantity: float
    metric: str

    @classmethod
    def from_dict(cls, data: Dict) -> Ingredient:
        return cls(
            name=data['name'],
            quantity=data['quantity'],
            metric=data['metric'],
        )

    def __add__(self, other) -> Ingredient:
        can_add = (
            isinstance(other, self.__class__)
            and other.name == self.name
            and other.metric == self.metric
        )
        if can_add:
            return Ingredient(
                name=self.name,
                quantity=self.quantity + other.quantity,
                metric=self.metric,
            )
        return NotImplemented


@dataclass
class Step:
    title: str
    description: str
    img: str = None  # assuming an url for the moment
    ingredients: List[Ingredient] = field(default_factory=list)

    @classmethod
    def from_dict(cls, data: Dict) -> Step:
        return cls(
            title=data['title'],
            description=data['description'],
            img=data.get('img', None),
            ingredients=[Ingredient.from_dict(ingredient_dict)
                         for ingredient_dict in data.get('ingredients', [])],
        )


_recipe_id_gen = (n for n in range(10**10))


@dataclass
class Recipe:
    name: str
    description: str
    id: int = None
    steps: List[Step] = field(default_factory=list)

    def __post_init__(self):
        """Ensure that instance creation results in a consistent state."""
        if self.id is None:
            self.id = next(_recipe_id_gen)

    @property
    def ingredients(self) -> List[Ingredient]:
        """Return the ingredients used by each step."""
        res = {}
        for step in self.steps:
            for ingredient in step.ingredients:
                if ingredient.name not in res:
                    res[ingredient.name] = ingredient
                else:
                    res[ingredient.name] += ingredient
        return list(res.values())

    @classmethod
    def from_dict(cls, data: Dict) -> Recipe:
        return cls(
            id=data.get('id', None),
            name=data['name'],
            description=data['description'],
            steps=[Step.from_dict(step_dict)
                   for step_dict in data.get('steps', [])],
        )

    @classmethod
    def random(cls) -> Recipe:
        return cls(
            name='some name ' + 'bli bla ' * random.randint(0, 5),
            description='random description ' + 'bla bla ' * random.randint(0, 10)
        )

    def as_dict(self) -> Dict:
        return asdict(self)
