"""Module providing an SQL implementation of :py:class:`Repository`.

To perform the SQL calls & stuff, this needs to map the :py:mod:`model`
classes to ORM functionalities provided by SQLAlchemy.

To use all this, just instantiate :py:class:`SQLRepository`, and provide it
with the database URL. By default, it is created in memory.

"""

import sqlalchemy as sa
from sqlalchemy.orm import mapper, relationship, sessionmaker
from sqlalchemy.orm.exc import NoResultFound

from .. import model
from ..repository import RecipeNotFound, Repository

DEFAULT_DB_URL: str = 'sqlite:///:memory:'

metadata = sa.MetaData()

ingredients = sa.Table(
    'ingredients', metadata,
    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('step_id', sa.Integer, sa.ForeignKey('steps.id')),
    sa.Column('name', sa.String(55), nullable=False),
    sa.Column('quantity', sa.Float, nullable=False),
    sa.Column('metric', sa.String(50), nullable=False),
)
steps = sa.Table(
    'steps', metadata,
    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('recipe_id', sa.Integer, sa.ForeignKey('recipes.id')),
    sa.Column('order', sa.Integer, nullable=False),
    sa.Column('title', sa.String(255), nullable=False),
    sa.Column('description', sa.String(1000), nullable=False),
)
recipes = sa.Table(
    'recipes', metadata,
    sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
    sa.Column('name', sa.String(100), nullable=False),
    sa.Column('description', sa.String(1000), nullable=False),
)


mapper(
    model.Ingredient, ingredients,
)
mapper(
    model.Step, steps, properties={
        'ingredients': relationship(model.Ingredient, backref='step'),
    }
)
mapper(
    model.Recipe, recipes, properties={
        'steps': relationship(model.Step, backref='recipe')
    }
)


class SQLRepository(Repository):
    def __init__(self, url: str = DEFAULT_DB_URL):
        self.engine = sa.create_engine(url)
        self.Session = sessionmaker(bind=self.engine)

    def init(self):
        # ensure that the tables are present
        metadata.create_all(bind=self.engine)

    def create_recipe(self, recipe_data):
        recipe = model.Recipe(**recipe_data)
        self.add_recipe(recipe)
        return recipe.id

    def add_recipe(self, recipe):
        session = self.Session()
        session.add(recipe)
        session.commit()

    def get_recipes(self):
        session = self.Session()
        return list(session.query(model.Recipe).all())

    def get_recipe(self, recipe_id):
        session = self.Session()
        try:
            return session.query(model.Recipe).filter_by(id=recipe_id).one()
        except NoResultFound:
            raise RecipeNotFound(f"no recipe with id={recipe_id}")
