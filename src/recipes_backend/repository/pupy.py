"""Module providing a pure-Python implementation of :py:class:`Repository`.

This relies on a Dictionary instance.

"""

from typing import Dict

from recipes_backend import model
from recipes_backend.repository import Repository, RecipeNotFound


class DictRepository(Repository):
    def __init__(self):
        self._recipes: Dict[int, model.Recipe] = {}

    def init(self):
        # nothing to initialize that has side effects
        pass

    def add_recipe(self, recipe):
        self._recipes[recipe.id] = recipe

    def get_recipes(self):
        return list(self._recipes.values())

    def get_recipe(self, recipe_id):
        try:
            return self._recipes[recipe_id]
        except KeyError:
            raise RecipeNotFound(f"no recipe with id={recipe_id}")

    def create_recipe(self, recipe_data):
        recipe = model.Recipe(**recipe_data)
        self.add_recipe(recipe)
        return recipe.id