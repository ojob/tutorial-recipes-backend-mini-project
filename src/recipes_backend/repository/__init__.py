"""Module defining the data storage interface.

:py:class:`Repository` is abstract, and is used as the interface for uses
of repository instances over the code (i.e. instances attributes types
shall just expect some kine of :py:class:`Repository` sub-class, and
nothing more.

"""

import abc
from typing import Dict, List

from recipes_backend import model


class RecipeNotFound(Exception):
    """Exception for recipe not found."""


class Repository(abc.ABC):

    @abc.abstractmethod
    def init(self) -> None:
        """Initialize the repo, including any side effects."""
        raise NotImplementedError()

    @abc.abstractmethod
    def add_recipe(self, recipe: model.Recipe) -> None:
        raise NotImplementedError()

    def add_test_data(self) -> None:
        recipe = model.Recipe.random()
        self.add_recipe(recipe)

    @abc.abstractmethod
    def get_recipes(self) -> List[model.Recipe]:
        raise NotImplementedError()

    @abc.abstractmethod
    def get_recipe(self, recipe_id: int) -> model.Recipe:
        raise NotImplementedError()

    @abc.abstractmethod
    def create_recipe(self, recipe_data: Dict) -> int:
        """Create a new recipe from dictionary data."""
        raise NotImplementedError()
