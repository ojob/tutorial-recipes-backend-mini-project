"""Module providing the web server application.

It is based on Flask, and consumes the authentication service to interact
with the data model and storage.

"""

from typing import Tuple

import flask
from flask.json import jsonify

from .repository import RecipeNotFound, Repository
from .repository.pupy import DictRepository

FlaskReturnType = Tuple[str, int]


def create_app(
        repo: Repository = None,
        with_test_data: bool = False,
) -> flask.Flask:
    app = flask.Flask(__name__)

    if repo is None:
        repo = DictRepository()
    app.repo = repo
    app.repo.init()
    if with_test_data:
        app.repo.add_test_data()

    @app.route('/')
    def home() -> FlaskReturnType:
        return jsonify(dict(message="hello world")), 200

    def recipes_get_endpoint() -> FlaskReturnType:
        recipes = [dict(id=recipe.id, name=recipe.name)
                   for recipe in repo.get_recipes()]
        if recipes:
            return jsonify(dict(message='found', data=recipes)), 200
        return jsonify(dict(message='No recipe')), 200

    def recipes_post_endpoint() -> FlaskReturnType:
        recipe_id = app.repo.create_recipe(flask.request.json)
        return jsonify(dict(message='created', data=recipe_id)), 201

    @app.route('/recipes', methods=['GET', 'POST'])
    def recipes_endpoint() -> FlaskReturnType:
        if flask.request.method == 'GET':
            return recipes_get_endpoint()
        else:
            return recipes_post_endpoint()

    @app.route('/recipes/<int:recipe_id>')
    def recipe_endpoint(recipe_id: int) -> FlaskReturnType:
        try:
            recipe = app.repo.get_recipe(recipe_id=recipe_id)
        except RecipeNotFound as exc:
            return jsonify(dict(message=str(exc))), 404
        else:
            return jsonify(dict(message='found', data=recipe.name)), 200

    @app.route('/recipes/<int:recipe_id>/details')
    def recipe_endpoint_full_details(recipe_id: int) -> FlaskReturnType:
        try:
            recipe = app.repo.get_recipe(recipe_id=recipe_id)
        except RecipeNotFound as exc:
            return jsonify(dict(message=str(exc))), 404
        else:
            return jsonify(dict(message='found', data=recipe.as_dict())), 200

    return app
